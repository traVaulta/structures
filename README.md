# Data structures

Sample implementations of popular advanced data structures used, including:
- Binary trees
  - standard
  - RBalanced

## Author

- [Matija Čvrk](https://hr.linkedin.com/in/matija-%C4%8Dvrk-1388b3101/)
