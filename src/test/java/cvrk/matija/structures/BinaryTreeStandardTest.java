package cvrk.matija.structures;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

class BinaryTreeStandardTest {

    @Test
    void should_create_tree_for_input_values_array_properly() throws Exception {
        int[] values = new int[]{18, 7, -1, 22, 3, 25, 10, 8, 26, -4, 27, -3, 11};
        BinaryTreeStandard.Node tree = BinaryTreeStandard.create(values);

        Assertions.assertThat(getTreeOutput(tree)).isEqualTo("""
        				27
        			26
        		25
        	22
        18
        			11
        		10
        			8
        	7
        			3
        		-1
        				-3
        			-4
        			""");
    }

    @Test
    void should_find_next_node_via_inorder_traversal_properly() {
        BinaryTreeStandard.Node tree = getTreeMocked();
        Assertions
            .assertThat(BinaryTreeStandard.findNextInOrder(tree, 7))
            .extracting("value")
            .isEqualTo(8);
    }

    @Test
    void should_remove_node_and_replace_with_next_inorder_candidate_properly() throws Exception {
        BinaryTreeStandard.Node tree = getTreeMocked();
        tree = BinaryTreeStandard.remove(tree, 7);

        Assertions.assertThat(getTreeOutput(tree)).isEqualTo("""
        				27
        			26
        		25
        	22
        18
        			11
        		10
        	8
        			3
        		-1
        				-3
        			-4
        			""");
    }

    @Test
    void should_count_hops_to_node_properly() {
        BinaryTreeStandard.Node tree = getTreeMocked();
        Assertions.assertThat(BinaryTreeStandard.countHops(tree, 0)).isEqualTo(3);
    }

    @Test
    void should_calc_depth_of_tree_properly() {
        BinaryTreeStandard.Node tree = getTreeMocked();
        Assertions.assertThat(BinaryTreeStandard.calcDepth(tree, 0)).isEqualTo(5);
    }

    @Test
    void should_rotate_left_properly() throws Exception {
        BinaryTreeStandard.Node tree = new BinaryTreeStandard.Node(4, null, null, null);
        BinaryTreeStandard.Node treeLeft = new BinaryTreeStandard.Node(1, null, null, tree);
        tree.setLeft(treeLeft);
        BinaryTreeStandard.Node treeRight = new BinaryTreeStandard.Node(7, null, null, tree);
        tree.setRight(treeRight);

        BinaryTreeStandard.Node childLeft = new BinaryTreeStandard.Node(5, null, null, treeRight);
        BinaryTreeStandard.Node childRight = new BinaryTreeStandard.Node(9, null, null, treeRight);
        treeRight.setLeft(childLeft);
        treeRight.setRight(childRight);

        Assertions
            .assertThat(getTreeOutput(tree))
            .isEqualTo("""
                		9
                	7
                		5
                4
                	1
                	""");

        tree = BinaryTreeStandard.rotateLeft(tree);

        Assertions
            .assertThat(getTreeOutput(tree))
            .isEqualTo("""
                	9
                7
                		5
                	4
                		1
                		""");
    }

    @Test
    void should_rotate_right_properly() throws Exception {
        BinaryTreeStandard.Node tree = new BinaryTreeStandard.Node(7, null, null, null);
        BinaryTreeStandard.Node treeLeft = new BinaryTreeStandard.Node(4, null, null, tree);
        tree.setLeft(treeLeft);
        BinaryTreeStandard.Node treeRight = new BinaryTreeStandard.Node(9, null, null, tree);
        tree.setRight(treeRight);

        BinaryTreeStandard.Node childLeft = new BinaryTreeStandard.Node(1, null, null, treeLeft);
        BinaryTreeStandard.Node childRight = new BinaryTreeStandard.Node(5, null, null, treeLeft);
        treeLeft.setLeft(childLeft);
        treeLeft.setRight(childRight);

        Assertions
            .assertThat(getTreeOutput(tree))
            .isEqualTo("""
                	9
                7
                		5
                	4
                		1
                		""");

        tree = BinaryTreeStandard.rotateRight(tree);

        Assertions
            .assertThat(getTreeOutput(tree))
            .isEqualTo("""
                		9
                	7
                		5
                4
                	1
                	""");
    }

    @Test
    void should_rotate_inner_node_without_side_effects() throws Exception {
        BinaryTreeStandard.Node tree = getTreeMocked();
        BinaryTreeStandard.rotateLeft(tree.getLeft());
        Assertions
            .assertThat(getTreeOutput(tree))
            .isEqualTo("""
        				27
        			26
        		25
        	22
        18
        		11
        	10
        			8
        		7
        				3
        			-1
        					-3
        				-4
        				""");
    }

    private static BinaryTreeStandard.Node getTreeMocked() {
        int[] values = new int[]{18, 7, -1, 22, 3, 25, 10, 8, 26, -4, 27, -3, 11};
        return BinaryTreeStandard.create(values);
    }

    private static String getTreeOutput(BinaryTreeStandard.Node tree) throws Exception {
        ByteArrayOutputStream managedOutput = new ByteArrayOutputStream();
        System.setOut(new PrintStream(managedOutput));

        BinaryTreeStandard.displayTree(tree, 0);
        managedOutput.flush();

        return managedOutput.toString();
    }
}
