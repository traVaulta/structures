package cvrk.matija.structures;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

class BinaryTreeStendhalTest {
    @Test
    void should_create_tree_properly() throws Exception {
        int[] values = new int[] { 18, 7, -1, 22, 3, 25, 10, 8, 26, -4, 27, -3, 11 };
        BinaryTreeStendhal.Node tree = BinaryTreeStendhal.create(values);

        Assertions.assertThat(getTreeOutput(tree)).isEqualTo("""
            			27
            		26
            			25
            	22
            			18
            				11
            		10
            			8
            7
            		3
            	-1
            			-3
            		-4
            		""");
    }

    private static String getTreeOutput(BinaryTreeStendhal.Node tree) throws Exception {
        ByteArrayOutputStream managedOutput = new ByteArrayOutputStream();
        System.setOut(new PrintStream(managedOutput));

        BinaryTreeStendhal.displayTree(tree, 0);
        managedOutput.flush();

        return managedOutput.toString();
    }
}
