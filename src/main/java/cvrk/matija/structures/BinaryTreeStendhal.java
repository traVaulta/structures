package cvrk.matija.structures;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

public class BinaryTreeStendhal {
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    static class Node {
        private Node left;
        private Node right;
        private boolean red;
        private int value;
        private Node parent;

        public boolean black() {
            return !this.red;
        }

        @Override
        public String toString() {
            return "" + value;
        }
    }

    public static void displayTree(Node root, int tab) {
        if (root != null) {
            displayTree(root.right, 1 + tab);
            System.out.println("\t".repeat(tab) + root);
            displayTree(root.left, 1 + tab);
        }
    }

    public static Node create(int[] values) {
        Node root = null;
        for (int value : values) {
            Node nodeNew = new Node(null, null, true, value, null);
            nodeNew.left = nodeNew.right = null;
            if (root == null) {
                nodeNew.parent = null;
                nodeNew.red = false;
                root = nodeNew;
            }
            else {
                for (Node node = root;;) {
                    int direction = Integer.compare(nodeNew.value, node.value);
                    Node parent = node;
                    if ((node = (direction <= 0) ? node.left : node.right) == null) {
                        nodeNew.parent = parent;
                        if (direction <= 0) {
                            parent.left = nodeNew;
                        } else {
                            parent.right = nodeNew;
                        }
                        root = insert(root, nodeNew);
                        break;
                    }
                }
            }
        }
        return root;
    }

    public static Node insert(Node root, Node node) {
        node.red = true;
        for (Node parent, grandParent, uncleLeft, uncleRight;;) {
            if ((parent = node.parent) == null) {
                node.red = false;
                return node;
            } else if (parent.black() || (grandParent = parent.parent) == null) {
                break;
            }
            List<Node> inter;
            if (parent == (uncleLeft = grandParent.left)) {
                uncleRight = grandParent.right;
                inter = update(root, node, parent, grandParent, uncleRight, false);
            } else {
                inter = update(root, node, parent, grandParent, uncleLeft, true);
            }
            root = inter.get(0);
            node = inter.get(1);
        }
        return root;
    }

    public static Node rotateLeft(Node root, Node parent) {
        Node siblingRight, grandParent, nephewRightLeft;
        if (parent != null && (siblingRight = parent.right) != null) {
            if ((nephewRightLeft = parent.right = siblingRight.left) != null)
                nephewRightLeft.parent = parent;
            if ((grandParent = siblingRight.parent = parent.parent) == null)
                (root = siblingRight).red = false;
            else if (grandParent.left == parent)
                grandParent.left = siblingRight;
            else
                grandParent.right = siblingRight;
            siblingRight.left = parent;
            parent.parent = siblingRight;
        }
        return root;
    }

    public static Node rotateRight(Node root, Node parent) {
        Node siblingLeft, grandParent, nephewLeftRight;
        if (parent != null && (siblingLeft = parent.left) != null) {
            if ((nephewLeftRight = parent.left = siblingLeft.right) != null)
                nephewLeftRight.parent = parent;
            if ((grandParent = siblingLeft.parent = parent.parent) == null)
                (root = siblingLeft).red = false;
            else if (grandParent.right == parent)
                grandParent.right = siblingLeft;
            else
                grandParent.left = siblingLeft;
            siblingLeft.right = parent;
            parent.parent = siblingLeft;
        }
        return root;
    }

    private static List<Node> update(Node root, Node node, Node parent, Node grandParent, Node uncle, boolean left) {
        if (uncle != null && uncle.red) {
            uncle.red = false;
            parent.red = false;
            grandParent.red = true;
            node = grandParent;
        } else {
            if (node == (left ? parent.left : parent.right)) {
                root = left ? rotateRight(root, node = parent) : rotateLeft(root, node = parent);
                grandParent = (parent = node.parent) == null ? null : parent.parent;
            }
            if (parent != null) {
                parent.red = false;
                if (grandParent != null) {
                    grandParent.red = true;
                    root = left ? rotateLeft(root, grandParent) : rotateRight(root, grandParent);
                }
            }
        }
        return List.of(root, node);
    }

}
