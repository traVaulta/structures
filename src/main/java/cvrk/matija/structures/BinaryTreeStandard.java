package cvrk.matija.structures;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public final class BinaryTreeStandard {
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    static class Node {
        private int value;
        private Node left;
        private Node right;
        private Node parent;

        @Override
        public String toString() {
            return "" + value;
        }
    }

    public static void displayTree(Node root, int t) {
        if (root != null) {
            displayTree(root.right, 1 + t);
            System.out.println("\t".repeat(t) + root.value);
            displayTree(root.left, 1 + t);
        }
    }

    public static void displayTreeInline(Node root) {
        if (root != null) {
            if (root.parent == null) System.out.print("INLINE: ");
            displayTreeInline(root.right);
            System.out.print(root.value + " ");
            displayTreeInline(root.left);
        }
    }

    public static Node create(int[] values) {
        Node root = new Node(values[0], null, null, null);
        for (int i = 1; i < values.length; i++) {
            root = insert(root, values[i]);
        }
        return root;
    }

    public static Node remove(Node root, int value) {
        if (root == null) {
            return root;
        } else if (root.value > value) {
            root.setLeft(remove(root.left, value));
            root.getLeft().setParent(root);
        } else if (root.value < value) {
            root.setRight(remove(root.left, value));
            root.getRight().setParent(root);
        } else {
            Node nextInOrder = findNextInOrder(root, value);
            root = replace(root, nextInOrder);
        }
        return root;
    }

    public static Node insert(Node root, int value) {
        if (root == null) {
            return new Node(value, null, null, null);
        } else if (root.value > value) {
            root.left = insert(root.left, value);
            root.left.setParent(root);
        } else if (root.value < value) {
            root.right = insert(root.right, value);
            root.right.setParent(root);
        }
        return root;
    }

    public static Node search(Node root, int value) {
        if (root == null) {
            return new Node(value, null, null, null);
        } else if (root.value > value) {
            return search(root.left, value);
        } else if (root.value < value) {
            return search(root.right, value);
        } else {
            return root;
        }
    }

    public static Node findNextInOrder(Node root, int value) {
        if (root == null || (root.left == null && root.right == null)) {
            return root;
        } else {
            Node leftCandidate = findNextInOrder(root.left, value);
            Node rightCandidate = findNextInOrder(root.right, value);
            if (leftCandidate == null) {
                return rightCandidate;
            } else if (rightCandidate == null) {
                return leftCandidate;
            } else {
                int leftDiff = Math.abs(leftCandidate.value - value);
                int rightDiff = Math.abs(rightCandidate.value - value);
                if (leftDiff > rightDiff) {
                    return rightCandidate;
                } else {
                    return leftCandidate;
                }
            }
        }
    }

    public static int countHops(Node root, int value) {
        if (root == null) {
            return -1;
        } else if (root.value > value) {
            return 1 + countHops(root.left, value);
        } else if (root.value < value) {
            return 1 + countHops(root.right, value);
        } else {
            return 0;
        }
    }

    public static int calcDepth(Node root, int depth) {
        if (root == null) {
            return depth;
        } else {
            int leftDepth = calcDepth(root.left, depth + 1);
            int rightDepth = calcDepth(root.right, depth + 1);
            return Math.max(leftDepth, rightDepth);
        }
    }

    public static Node rotateLeft(Node node) {
        Node parent = node.parent;
        boolean isRoot = parent == null;
        boolean leftToParent = !isRoot && parent.left != null && parent.left.equals(node);
        Node nodeRight = node.right;
        Node nrLeft = nodeRight.left;

        node.right = nrLeft;
        if (nrLeft != null) {
            nrLeft.parent = node;
        }
        node.parent = nodeRight;
        nodeRight.parent = parent;

        nodeRight.left = node;

        if (!isRoot && leftToParent) {
            parent.left = nodeRight;
        } else if (!isRoot && !leftToParent) {
            parent.right = nodeRight;
        }

        return nodeRight;
    }

    public static Node rotateRight(Node node) {
        Node parent = node.parent;
        boolean isRoot = parent == null;
        boolean leftToParent = !isRoot && parent.left != null && parent.left.equals(node);
        Node nodeLeft = node.left;
        Node nlRight = nodeLeft.right;

        node.left = nlRight;
        if (nlRight != null) {
            nlRight.parent = node;
        }
        node.parent = nodeLeft;
        nodeLeft.parent = parent;

        nodeLeft.right = node;

        if (!isRoot && leftToParent) {
            parent.left = nodeLeft;
        } else if (!isRoot && !leftToParent) {
            parent.right = nodeLeft;
        }

        return nodeLeft;
    }

    private static Node replace(Node target, Node replacement) {
        Node parent = target.getParent();
        Node oldParent = replacement.getParent();

        if (oldParent.getLeft() == replacement) {
            oldParent.setLeft(null);
        }
        if (oldParent.getRight() == replacement) {
            oldParent.setRight(null);
        }

        replacement.setParent(target.getParent());

        if (target.getLeft() != null) {
            replacement.setLeft(target.getLeft());
            target.getLeft().setParent(replacement);
        }
        if (target.getRight() != null) {
            replacement.setRight(target.getRight());
            target.getRight().setParent(replacement);
        }

        if (parent.getLeft() == target) {
            parent.setLeft(replacement);
        } else {
            parent.setRight(replacement);
        }

        return replacement;
    }
}
